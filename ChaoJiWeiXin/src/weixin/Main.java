/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package weixin;

import RTPower.RTFile;
import com.apple.eawt.Application;
import java.awt.Image;

/**
 *
 * @author jerry
 */
public class Main {

    /**
     * 在static 静态配置里面设置mac 的dock栏目显示的图标
     */
    static {
        //获得操作系统
        String OsName = System.getProperty("os.name");
        //是mac 就设置dock图标
        if (OsName.contains("Mac")) {
            Image icon_image = new GetIconImage().GetImage();
            //指定mac 的dock图标
            Application app = Application.getApplication();
            app.setDockIconImage(icon_image);
        }

    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        //运行前检测有没有images/face_images 目录
        RTFile.CreateDirectory("."+RTFile.FG+"images");
        RTFile.CreateDirectory("."+RTFile.FG+"images"+RTFile.FG+"face_images");
        new AdminMain().setVisible(true);

    }

}
